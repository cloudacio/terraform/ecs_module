variable "autoscale_enabled" {
  default     = false
  description = "Flag to set Auto Scale"
}

variable "service" {
  default     = null
  description = "Service name"
}

variable "cluster_name" {
  default     = null
  description = "Cluster name"
}

variable "environment" {
  default     = null
  description = "Environment"
}

variable "region" {
  default     = null
  description = "AWS Region"
}

variable "desired_count" {
  default     = 1
  description = "How many containers do we need"
}
