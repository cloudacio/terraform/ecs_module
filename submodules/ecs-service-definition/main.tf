resource "aws_ecs_service" "default" {
  name                               = var.service
  cluster                            = data.aws_ecs_cluster.default.id
  task_definition                    = var.task_definition
  desired_count                      = var.desired_count
  launch_type                        = "FARGATE"
  health_check_grace_period_seconds  = 300
  deployment_maximum_percent         = 200
  deployment_minimum_healthy_percent = 50
  propagate_tags                     = "SERVICE"

  tags = {
    environment = var.environment
  }

  network_configuration {
    security_groups  = [data.aws_security_group.default.id]
    subnets          = var.assign_public_ip == false ? data.aws_subnet_ids.private.ids : data.aws_subnet_ids.public.ids
    assign_public_ip = var.assign_public_ip
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.alb_target_group.arn
    container_name   = var.service
    container_port   = var.port
  }

  depends_on = [aws_lb.default]
}
