data "aws_iam_role" "ecsTaskExecutionRole" {
  name = "ecsTaskExecutionRole"
}

resource "aws_ecs_task_definition" "default" {
  count                    = var.efs_id == null ? 1 : 0
  family                   = var.family
  container_definitions    = var.container_definitions
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = data.aws_iam_role.ecsTaskExecutionRole.arn
  task_role_arn            = data.aws_iam_role.ecsTaskExecutionRole.arn

  tags = {
    environment = var.environment
  }
}


resource "aws_ecs_task_definition" "efs" {
  count                    = var.efs_id != null ? 1 : 0
  family                   = var.family
  container_definitions    = var.container_definitions
  requires_compatibilities = ["FARGATE"]
  network_mode             = "awsvpc"
  cpu                      = var.cpu
  memory                   = var.memory
  execution_role_arn       = data.aws_iam_role.ecsTaskExecutionRole.arn
  task_role_arn            = data.aws_iam_role.ecsTaskExecutionRole.arn

  tags = {
    environment = var.environment
  }

  volume {
    name = "efs-storage"

    efs_volume_configuration {
      file_system_id = var.efs_id
    }
  }
}