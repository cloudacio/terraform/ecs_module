locals {
  cluster_name = "${var.cluster_name}-${var.environment}"
  enabled      = var.autoscale_enabled == true && var.environment == "production" ? 1 : 0
}

resource "aws_appautoscaling_target" "main" {
  count              = local.enabled
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"
  min_capacity       = var.desired_count
  max_capacity       = 6
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_high_cpu_up" {
  count               = local.enabled
  alarm_name          = "${var.service}-cpu-scale-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 80
  alarm_actions       = [aws_appautoscaling_policy.cpu-up[count.index].arn]

  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = var.service
  }
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_high_mem_up" {
  count               = local.enabled
  alarm_name          = "${var.service}-mem-scale-up"
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 80
  alarm_actions       = [aws_appautoscaling_policy.mem-up[count.index].arn]

  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = var.service
  }
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_high_cpu_down" {
  count               = local.enabled
  alarm_name          = "${var.service}-cpu-scale-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "CPUUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 80
  alarm_actions       = [aws_appautoscaling_policy.cpu-down[count.index].arn]

  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = var.service
  }
}

resource "aws_cloudwatch_metric_alarm" "ecs_service_high_mem_down" {
  count               = local.enabled
  alarm_name          = "${var.service}-mem-scale-down"
  comparison_operator = "LessThanOrEqualToThreshold"
  evaluation_periods  = 1
  metric_name         = "MemoryUtilization"
  namespace           = "AWS/ECS"
  period              = 60
  statistic           = "Average"
  threshold           = 80
  alarm_actions       = [aws_appautoscaling_policy.mem-down[count.index].arn]

  dimensions = {
    ClusterName = local.cluster_name
    ServiceName = var.service
  }
}

resource "aws_appautoscaling_policy" "cpu-up" {
  count              = local.enabled
  name               = "${var.service}-cpu-scale-up"
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = "300"
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = [aws_appautoscaling_target.main]
}

resource "aws_appautoscaling_policy" "mem-up" {
  count              = local.enabled
  name               = "${var.service}-mem-scale-up"
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = 1
    }
  }

  depends_on = [aws_appautoscaling_target.main]
}

resource "aws_appautoscaling_policy" "cpu-down" {
  count              = local.enabled
  name               = "${var.service}-cpu-scale-down"
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = [aws_appautoscaling_target.main]
}

resource "aws_appautoscaling_policy" "mem-down" {
  count              = local.enabled
  name               = "${var.service}-mem-scale-down"
  service_namespace  = "ecs"
  resource_id        = "service/${local.cluster_name}/${var.service}"
  scalable_dimension = "ecs:service:DesiredCount"

  step_scaling_policy_configuration {
    adjustment_type         = "ChangeInCapacity"
    cooldown                = 300
    metric_aggregation_type = "Average"

    step_adjustment {
      metric_interval_lower_bound = 0
      scaling_adjustment          = -1
    }
  }

  depends_on = [aws_appautoscaling_target.main]
}
