variable "service" {
  default     = null
  description = "Service Name"
}

variable "region" {
  default     = null
  description = "AWS Region"
}

variable "environment" {
  default     = null
  description = "Environment"
}

variable "cluster_name" {
  default     = null
  description = "Cluster name"
}

variable "desired_count" {
  default     = 1
  description = "How many containers do we need"
}

variable "task_definition" {
  default     = null
  description = "Task Definition ARN"
}

variable "port" {
  default     = 80
  description = "Container Port"
}

variable "health_point" {
  default     = "/"
  description = "Custom Health Point"
}

variable "vpc_id" {
  default     = null
  description = "VPC id"
}

variable "alb_security_group" {
  default     = null
  description = "Security Groups id for ALB"
}

variable "root_domain" {
  default     = null
  description = "Service root domain"
}

variable "idle_timeout" {
  default     = 60
  description = "The time in seconds that the connection is allowed to be idle."
}

variable "assign_public_ip" {
  default     = false
  description = "Service root domain"
}