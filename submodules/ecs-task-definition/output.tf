output "arn" {
  description = "Task Definirion ARN"
  value       = join("", aws_ecs_task_definition.default.*.arn)
}

output "arn_efs" {
  description = "Task Definirion ARN"
  value       = join("", aws_ecs_task_definition.efs.*.arn)
}