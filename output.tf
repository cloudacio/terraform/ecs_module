output "id" {
  description = "ECS service id"
  value       = module.service_definition.id
}

output "name" {
  description = "ECS service name"
  value       = module.service_definition.name
}

output "version" {
  description = "ECS service version"
  value       = local.version
}

output "lb_zone_id" {
  description = "LB zone id"
  value       = module.service_definition.lb_zone_id
}

output "lb_dns_name" {
  description = "LB DNS name"
  value       = module.service_definition.lb_dns_name
}

output "service_dns_name" {
  description = "Service DNS name"
  value       = aws_route53_record.service_dns.name
}

