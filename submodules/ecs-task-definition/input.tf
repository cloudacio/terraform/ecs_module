variable "family" {
  default     = null
  description = "Service-Environment"
}

variable "container_definitions" {
  default     = null
  description = "Json of the Container Definition"
}

variable "cpu" {
  default     = 256
  description = "CPU allocation"
}

variable "memory" {
  default     = 512
  description = "Memory allocation"
}

variable "environment" {
  default     = null
  description = "Environment"
}

variable "efs_id" {
  default     = null
  description = "EFS id"
}