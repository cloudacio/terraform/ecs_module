variable "vpc_id" {
  default     = null
  description = "VPC id"
}

variable "service" {
  default     = null
  description = "Service name"
}

variable "cluster_name" {
  default     = null
  description = "Cluster name"
}

variable "service_public_name" {
  default     = null
  description = "Service public name used in DNS record"
}

variable "service_version" {
  default     = "latest"
  description = "Service version"
}

variable "docker_image" {
  default     = null
  description = "Service docker image"
}

variable "docker_cmd" {
  type        = list(any)
  description = "The command that is passed to the container"
  default     = [""]
}

variable "health_point" {
  default     = "/"
  description = "Custom Health Point"
}

variable "region" {
  default     = null
  description = "AWS Region"
}

variable "environment" {
  default     = null
  description = "Environment"
}

variable "root_domain" {
  default     = null
  description = "Service root domain"
}

variable "port" {
  default     = 80
  description = "Exposed port"
}

variable "cpu" {
  default     = 256
  description = "CPU allocation"
}

variable "memory" {
  default     = 512
  description = "Memory allocation"
}

variable "desired_count" {
  default     = 1
  description = "How many containers do we need"
}

variable "env_vars" {
  type        = list(any)
  description = "Service runtime environment variables"
  default     = []
}

variable "secrets" {
  type        = list(any)
  description = "The secrets to pass to the container"
  default     = []
}

variable "autoscale_enabled" {
  default     = false
  description = "Flag to set Auto Scale"
}

variable "idle_timeout" {
  default     = 60
  description = "The time in seconds that the connection is allowed to be idle."
}

variable "assign_public_ip" {
  default     = false
  description = "Service root domain"
}

variable "alb_security_group" {
  default     = null
  description = "Security Groups id for ALB"
}

variable "efs_id" {
  default     = null
  description = "EFS id"
}

variable "data_mount" {
  default     = "/var/www/html"
  description = "EFS directory path to mount into the container"
}