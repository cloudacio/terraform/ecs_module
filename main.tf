locals {
  service     = var.service
  environment = var.environment
  version     = var.service_version
  service_id  = "${local.service}-${var.environment}"

  urls = {
    "development" = var.service_public_name == null ? "dev.${var.root_domain}" : "${var.service_public_name}-dev.${var.root_domain}"
    "production"  = var.service_public_name == null ? var.root_domain : "${var.service_public_name}.${var.root_domain}"
  }
}

/*====
Container Definitions
======*/

module "container_definition" {
  source           = "./submodules/ecs-container-definition"
  container_name   = local.service_id
  container_image  = var.docker_image
  region           = var.region
  container_memory = var.memory
  container_cpu    = var.cpu
  command          = var.docker_cmd
  env_vars         = var.env_vars
  secrets          = var.secrets
  efs_id           = var.efs_id

  port_mappings = [{
    "containerPort" = var.port
    "hostPort"      = var.port
    "protocol"      = "tcp"
  }]

  mount_points = [{
    "sourceVolume"  = "efs-storage",
    "containerPath" = var.data_mount
  }]
}

/*====
Service Definition
======*/

module "service_definition" {
  source             = "./submodules/ecs-service-definition"
  service            = local.service_id
  environment        = local.environment
  region             = var.region
  desired_count      = var.desired_count
  root_domain        = var.root_domain
  alb_security_group = var.alb_security_group
  health_point       = var.health_point
  task_definition    = var.efs_id == null ? module.task_definition.arn : module.task_definition.arn_efs
  port               = var.port
  idle_timeout       = var.idle_timeout
  assign_public_ip   = var.assign_public_ip
  vpc_id             = var.vpc_id
  cluster_name       = var.cluster_name
}

/*====
AutoScaling Definition
======*/

module "autoscaling_definition" {
  source            = "./submodules/ecs-autoscaling-definition"
  service           = module.service_definition.name
  environment       = local.environment
  region            = var.region
  cluster_name      = var.cluster_name
  desired_count     = var.desired_count
  autoscale_enabled = var.autoscale_enabled
}

/*====
Task Definitions
======*/

module "task_definition" {
  source                = "./submodules/ecs-task-definition"
  family                = local.service_id
  container_definitions = module.container_definition.json
  cpu                   = var.cpu
  memory                = var.memory
  environment           = var.environment
  efs_id                = var.efs_id
}

/*====
Cloudwatch Log Group
======*/

resource "aws_cloudwatch_log_group" "default" {
  name              = "/ecs/${local.service_id}"
  retention_in_days = "14"
}

/*====
Route53 DNS
======*/

data "aws_route53_zone" "service_zone" {
  name = "${var.root_domain}."
}

resource "aws_route53_record" "service_dns" {
  zone_id = data.aws_route53_zone.service_zone.zone_id
  name    = local.urls[var.environment]
  type    = "A"

  alias {
    name                   = module.service_definition.lb_dns_name
    zone_id                = module.service_definition.lb_zone_id
    evaluate_target_health = true
  }
}

