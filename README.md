# Module for creation of an ECS Fargate service

This module will create: 

- ECS fargate service
- ECS task definition
- ECS tasks
- ALB
- Target Group
- DNS record as an alias of the ALB
- OPTIONAL: will mount an EFS into any `data_mount` inside the container

### Prerequisites
- ECS fagate cluster with default iam role `ecsTaskExecutionRole`
- SSL cert issued in the AWS Certificate manager called `*.domain.com`
- A defaul security group call `default-${var.environment}`
- Subnets with tags `access:private` and/or `access:public`
- If mounting an EFS, the EFS needs to exist and the files to host should be in the data_mount dir


### Usage

```
module "ecs_service_example1" {
  source              = "git::ssh://git@gitlab.com/cloudacio/terraform/ecs_module.git?ref=v<tag>"
  service             = "example1"
  cluster_name        = "jpdentone001"
  service_version     = "latest"
  region              = "us-east-1"
  environment         = "development"
  root_domain         = "dev.cloudacio.com"
  service_public_name = "jptesting-ecs"
  vpc_id              = "vpc-e9826294"
  alb_security_group  = "sg-0fe90573fd9e461aa"
  docker_image        = "nginx"
  efs_id              = "fs-0ccbddb8"    ##remove if not needed
  data_mount          = "/usr/share/nginx/html" ##remove if not needed
  assign_public_ip    = true
  port                = 80
  desired_count       = 1

  env_vars = [
    {
      name  = "ENV"
      value = "development"
    },
  ]
}

