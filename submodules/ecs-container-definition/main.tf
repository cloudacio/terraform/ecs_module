locals {
  container_definition = {
    name                   = var.container_name
    image                  = var.container_image
    memory                 = "memory_sentinel_value"
    memoryReservation      = "memory_reservation_sentinel_value"
    cpu                    = "cpu_sentinel_value"
    essential              = var.essential
    entryPoint             = var.entrypoint
    command                = var.command
    workingDirectory       = var.working_directory
    readonlyRootFilesystem = var.readonly_root_filesystem
    mountPoints            = var.efs_id == null ? [] : var.mount_points
    dnsServers             = var.dns_servers
    ulimits                = var.ulimits
    repositoryCredentials  = var.repository_credentials
    links                  = var.links
    volumesFrom            = var.volumes_from
    user                   = var.user
    dependsOn              = var.container_depends_on
    stopTimeout            = "stop_timeout_sentinel_value"
    healthCheck            = var.healthcheck
    portMappings           = var.port_mappings
    dockerLabels           = "docker_labels_sentinel_value"

    logConfiguration = {
      logDriver = "awslogs"
      options = {
        "awslogs-region"        = var.region
        "awslogs-group"         = "/ecs/${var.container_name}"
        "awslogs-stream-prefix" = "ecs"
      }
    }
    environment = "environment_sentinel_value"
    secrets     = "secrets_sentinel_value"
  }

  env_vars = var.env_vars
  secrets  = var.secrets
}

