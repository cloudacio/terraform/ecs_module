data "aws_subnet_ids" "public" {
  vpc_id = var.vpc_id

  tags = {
    access = "public"
  }
}

data "aws_subnet_ids" "private" {
  vpc_id = var.vpc_id

  tags = {
    access = "private"
  }
}

data "aws_acm_certificate" "certiticate" {
  domain   = "*.${var.root_domain}"
  statuses = ["ISSUED"]
}

data "aws_ecs_cluster" "default" {
  cluster_name = "${var.cluster_name}-${var.environment}"
}

data "aws_security_group" "default" {
  vpc_id = var.vpc_id
  name   = "default-${var.environment}"
}