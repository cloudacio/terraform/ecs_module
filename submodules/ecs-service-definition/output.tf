output "id" {
  description = "ECS service id"
  value       = aws_ecs_service.default.id
}

output "name" {
  description = "ECS service name"
  value       = aws_ecs_service.default.name
}

output "lb_zone_id" {
  description = "LB zone id"
  value       = aws_lb.default.zone_id
}

output "lb_dns_name" {
  description = "LB DNS name"
  value       = aws_lb.default.dns_name
}
